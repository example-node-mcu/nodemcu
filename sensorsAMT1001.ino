int RHpin = A0;

int RTpin = D2;

float RH = 0;

float RT = 0;

void setup() {

   Serial.begin(9600);

}

void loop() {

   RH = analogRead(RHpin);

   RT = analogRead(RTpin);

   RT = (((RT*5)/1023)*80)/0.8;

   RH = (((RH*5)/1023)*100)/3;

   Serial.print("humidity: ");

   Serial.print(RH);

   Serial.print(" %");

   Serial.print("\t\t");

   Serial.print("temperature: ");

   Serial.print(RT);

   Serial.println(" C");

   delay(100);

}

 